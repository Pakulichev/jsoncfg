package = "jsoncfg"
version = "1.1.1-1"
source = {
   url = "git+https://gitlab.com/Pakulichev/jsoncfg.git",
   tag = "v.1.1.1"
}
description = {
   summary = "Work easier with JSON",
   detailed = "",
   homepage = "https://gitlab.com/Pakulichev/jsoncfg",
   license = "MIT"
}
dependencies = {
   "lua >= 5.1, < 5.4"
}
build = {
   type = "builtin",
   modules = {
      jsoncfg = "lua/jsoncfg.lua"
   }
}
